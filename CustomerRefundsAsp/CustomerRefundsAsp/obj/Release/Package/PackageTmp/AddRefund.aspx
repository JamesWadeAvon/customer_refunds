﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddRefund.aspx.cs" Inherits="CustomerRefundsAsp.AddRefund" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <link href="./Content/Refunds.css" type="text/css" rel="stylesheet" />
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function updateTotal() {
            // For live-calculating total amount
            let refund = document.getElementById("txtRefund").value;
            let postage = document.getElementById("txtPostage").value;
            let tax = document.getElementById("txtTax").value;
            // but why does it keep coming up as NaN???            
            let total = Number(refund) + Number(postage) + Number(tax);
            let totalid = document.getElementById("txtTotal");
            totalid.value = total.toFixed(2);
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="controlContainer">

            <div class="formRow">
                <label>Location:</label>
                <asp:DropDownList ID="dbBranch" runat="server">
                    <asp:ListItem Value="AT">Atlanta</asp:ListItem>
                    <asp:ListItem Value="MG">Morton Grove</asp:ListItem>
                    <asp:ListItem Value="NW">Newark</asp:ListItem>
                    <asp:ListItem Value="PA">Pasadena</asp:ListItem>
                </asp:DropDownList>
                <label>Refund Type:</label>
                <asp:DropDownList ID="dbRefType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="DEC">Deceased</asp:ListItem>
                    <asp:ListItem Value="DIS">Discontinued</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="formRow">
                <label>First Name:</label>
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                <label>Last Name:</label>
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            </div>
            <div class="formRow">
                <label>Address 1:</label>
                <asp:TextBox ID="txtAddress1" runat="server"></asp:TextBox>
            </div>
            <div class="formRow">
                <label>Address 2:</label>
                <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox>
            </div>
            <div class="formRow">
                <label>City:</label>
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                <label>State:</label>
                <asp:DropDownList ID="dbState" runat="server" DataSourceID="CustRefDB" DataTextField="State" DataValueField="StateCode">
                </asp:DropDownList>
                <asp:SqlDataSource ID="CustRefDB" runat="server" ConnectionString="<%$ ConnectionStrings:CustRefsConnectionString %>" SelectCommand="SELECT * FROM [US_States]"></asp:SqlDataSource>
            </div>
            <div class="formRow">
                <label>Zip Code:</label>
                <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                <label>Allergy</label>
                <asp:CheckBox CssClass="AllergyBox" ID="chkAllergy" runat="server" Height="50px" Width="50px"/>
            </div>
            <div class="formRow">
                <label>Phone Number:</label>
                <asp:TextBox ID="txtPhoneNum" runat="server"></asp:TextBox>
            </div>
            <br />
            <div class="formRow">
                <label>Refund:</label>
                <asp:TextBox ID="txtRefund" runat="server" OnKeyUp="updateTotal()"></asp:TextBox>
                <label>Returned Mdse:</label>
                <asp:TextBox ID="txtReturnedMdse" runat="server"></asp:TextBox>
            </div>
            <div class="formRow">
                <label>Postage:</label>
                <asp:TextBox ID="txtPostage" runat="server" OnKeyUp="updateTotal()"></asp:TextBox>
                <label>Cust Discount:</label>
                <asp:TextBox ID="txtCustDiscount" runat="server"></asp:TextBox>
            </div>
            <div class="formRow">
                <label>Tax:</label>
                <asp:TextBox ID="txtTax" runat="server" OnKeyUp="updateTotal()"></asp:TextBox>
                <label>Total Amount:</label>
                <asp:TextBox ID="txtTotal" runat="server" ForeColor="Red"></asp:TextBox>
            </div>
            <div class="formRow">
                <label>Notes:</label>
                <asp:TextBox ID="txtNotes" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnClear" runat="server" Text="Clear Form" OnClick="btnClear_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
            </div>


        </div>
     
    </form>
</body>
</html>
