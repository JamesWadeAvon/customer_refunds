﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CustomerRefundsAsp._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Customer Refunds</h1>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Add a New Customer Refund</h2>
            <p>
                <a class="btn btn-default" href="./AddRefund.aspx">Add Refund</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>View and Modify Pending Refunds</h2>
            <p>
                <a class="btn btn-default" href="./PendingRefunds.aspx">View Pending</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Reporting</h2>
            <p>
                This will take you to the reporting server.
            </p>
            <p>
                <a class="btn btn-default" href="http://sprmscsql/Reports/browse/CustomerRefunds">View Reports</a>
            </p>
        </div>
    </div>

</asp:Content>
