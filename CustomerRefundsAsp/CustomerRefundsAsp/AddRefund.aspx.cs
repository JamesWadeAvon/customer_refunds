﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace CustomerRefundsAsp
{
    public partial class AddRefund : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Int64 VoucherNum = generateVoucherNumber();
            decimal parsed;
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CustRefsConnectionString"].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd;
            string insertcmd = "INSERT INTO Refunds (FirstName, LastName, Branch, Address1, Address2, City, State, ZipCode, PhoneNumber, EntryDate, CustomerPrice, ReturnedMdse, CustomerDisc, Postage, Tax, TotalRefund, VoucherNum, RefundType, Allergy, Notes, EnteredBy) " +
                "VALUES(@FirstName, @LastName, @Branch, @Address1, @Address2, @City, @State, @ZipCode, @PhoneNumber, @EntryDate, @CustomerPrice, @ReturnedMdse, @CustomerDisc, @Postage, @Tax, @TotalRefund, @VoucherNum, @RefundType, @Allergy, @Notes, @EnteredBy);";


            cmd = new SqlCommand(insertcmd, conn);
            cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            cmd.Parameters.AddWithValue("@Branch", dbBranch.SelectedValue);
            cmd.Parameters.AddWithValue("@Address1", txtAddress1.Text);
            cmd.Parameters.AddWithValue("@Address2", txtAddress2.Text);
            cmd.Parameters.AddWithValue("@City", txtCity.Text);
            cmd.Parameters.AddWithValue("@State", dbState.SelectedValue);
            cmd.Parameters.AddWithValue("@ZipCode", txtZip.Text);
            cmd.Parameters.AddWithValue("@PhoneNumber", txtPhoneNum.Text);
            cmd.Parameters.AddWithValue("@EntryDate", System.DateTime.Now);
            if (decimal.TryParse(txtRefund.Text, out parsed))
            {
                cmd.Parameters.AddWithValue("@CustomerPrice", parsed);
            } else { cmd.Parameters.AddWithValue("@CustomerPrice", 0); }

            if (decimal.TryParse(txtReturnedMdse.Text, out parsed))
            {
                cmd.Parameters.AddWithValue("@ReturnedMdse", parsed);
            } else { cmd.Parameters.AddWithValue("@ReturnedMdse", 0); }

            if (decimal.TryParse(txtCustDiscount.Text, out parsed))
            {
                cmd.Parameters.AddWithValue("@CustomerDisc", parsed);
            } else { cmd.Parameters.AddWithValue("@CustomerDisc", 0); }

            if (decimal.TryParse(txtPostage.Text, out parsed))
            {
                cmd.Parameters.AddWithValue("@Postage", parsed);
            } else { cmd.Parameters.AddWithValue("@Postage", 0); }

            if (decimal.TryParse(txtTax.Text, out parsed))
            {
                cmd.Parameters.AddWithValue("@Tax", parsed);
            } else { cmd.Parameters.AddWithValue("@Tax", 0); }

            if (decimal.TryParse(txtTotal.Text, out parsed))
            {
                cmd.Parameters.AddWithValue("@TotalRefund", parsed);
            } else { cmd.Parameters.AddWithValue("@TotalRefund", 0); }

            cmd.Parameters.AddWithValue("@VoucherNum", VoucherNum);
            cmd.Parameters.AddWithValue("@RefundType", dbRefType.SelectedValue);
            cmd.Parameters.AddWithValue("@Allergy", chkAllergy.Checked);
            cmd.Parameters.AddWithValue("@Notes", txtNotes.Text);
            cmd.Parameters.AddWithValue("@EnteredBy", User.Identity.Name);

            conn.Open();
            da.InsertCommand = cmd;
            da.InsertCommand.ExecuteNonQuery();

            cmd.Dispose();
            conn.Close();


            clearForm();
            Response.Write("VoucherNum " + VoucherNum.ToString() + " added to database.");
        }

        private Int64 generateVoucherNumber()
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CustRefsConnectionString"].ConnectionString;
            Int64 VoucherNum = 0;
            SqlConnection conn;
            string getVoucher = "SELECT MAX(VoucherNum) FROM Refunds;";
            SqlCommand getVoucherCommand;
            SqlDataReader getVoucherReader;

            conn = new SqlConnection(connectionString);
            conn.Open();

            getVoucherCommand = new SqlCommand(getVoucher, conn);
            getVoucherReader = getVoucherCommand.ExecuteReader();

            while (getVoucherReader.Read())
            {
                if (getVoucherReader.IsDBNull(0))
                {
                    VoucherNum = 0;
                }
                else
                {
                    VoucherNum = getVoucherReader.GetInt64(0);
                }
            }

            VoucherNum++;

            getVoucherReader.Close();
            getVoucherCommand.Dispose();
            conn.Close();

            return VoucherNum;
        }

        private void clearForm()
        {
            foreach (Control ctrl in form1.Controls)
            {
                if (ctrl is System.Web.UI.WebControls.TextBox)
                {
                    (ctrl as TextBox).Text = "";
                }

                if (ctrl is System.Web.UI.WebControls.DropDownList)
                {
                    (ctrl as DropDownList).SelectedIndex = 0;
                }

                if (ctrl is System.Web.UI.WebControls.CheckBox)
                {
                    (ctrl as CheckBox).Checked = false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            clearForm();
        }
    }
}


   