﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PendingRefunds.aspx.cs" Inherits="CustomerRefundsAsp.PendingRefunds" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="gridPending" runat="server" AutoGenerateColumns="False" DataSourceID="PendingRefs" OnRowCommand="gridPending_RowCommand">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnModifyPending" runat="server" Text="Modify" CommandName="ModifyButton" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VoucherNum" HeaderText="Voucher Number" SortExpression="VoucherNum" />
                    <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                    <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                    <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" SortExpression="EntryDate" />
                    <asp:BoundField DataField="TotalRefund" HeaderText="Total Refund" SortExpression="TotalRefund" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="PendingRefs" runat="server" ConnectionString="<%$ ConnectionStrings:CustRefsConnectionString %>" SelectCommand="SELECT [FirstName], [LastName], [VoucherNum], [EntryDate], [TotalRefund] FROM [Refunds] WHERE ([SentToAPDate] IS NULL)"></asp:SqlDataSource>
        </div>
        <div>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
        </div>
    </form>
</body>
</html>
