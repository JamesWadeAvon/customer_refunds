﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace CustomerRefundsAsp
{
    public partial class ModifyPending : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string ModifyVoucher = Request.QueryString["VoucherNum"];
                FillPageWithData(Convert.ToInt64(ModifyVoucher));
            }
        }

        protected void FillPageWithData(Int64 ModifyVoucher)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CustRefsConnectionString"].ConnectionString);
            SqlDataReader dr;
            string sql = "SELECT FirstName, LastName, Branch, Address1, Address2, City, State, ZipCode, PhoneNumber, EntryDate, CustomerPrice, ReturnedMdse, CustomerDisc, Postage, Tax, TotalRefund, VoucherNum, RefundType, Allergy, Notes, EnteredBy FROM Refunds WHERE VoucherNum = " + ModifyVoucher + ";";
            SqlCommand command = new SqlCommand(sql, conn);
            object[] RefundInfo = new object[21];

            conn.Open();
            dr = command.ExecuteReader();
            while (dr.Read())
            {
                dr.GetValues(RefundInfo);
            }

            /* For testing
            for (int i = 0; i < RefundInfo.Length; i++)
            {
                Response.Write(RefundInfo[i].ToString() + "</br>");
            }
            */

            conn.Close();



            
            txtFirstName.Text = RefundInfo[0].ToString();
            txtLastName.Text = RefundInfo[1].ToString();
            dbBranch.SelectedValue = RefundInfo[2].ToString();
            txtAddress1.Text = RefundInfo[3].ToString();
            txtAddress2.Text = RefundInfo[4].ToString();
            txtCity.Text = RefundInfo[5].ToString();
            dbState.SelectedValue = RefundInfo[6].ToString();
            txtZip.Text = RefundInfo[7].ToString();
            txtPhoneNum.Text = RefundInfo[8].ToString();
            litLastModifiedDate.Text = RefundInfo[9].ToString();
            txtRefund.Text = RefundInfo[10].ToString();
            txtReturnedMdse.Text = RefundInfo[11].ToString();
            txtCustDiscount.Text = RefundInfo[12].ToString();
            txtPostage.Text = RefundInfo[13].ToString();
            txtTax.Text = RefundInfo[14].ToString();
            txtTotal.Text = RefundInfo[15].ToString();
            litVoucherNum.Text = RefundInfo[16].ToString();
            dbRefType.SelectedValue = RefundInfo[17].ToString();
            chkAllergy.Checked = Convert.ToBoolean(RefundInfo[18]);
            txtNotes.Text = RefundInfo[19].ToString();
            litLastModifiedUser.Text = RefundInfo[20].ToString();
                      
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Int64 VoucherNum = Convert.ToInt64(litVoucherNum.Text);
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CustRefsConnectionString"].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd;
            string updatecmd = "UPDATE Refunds SET FirstName = @FirstName, LastName = @LastName, Branch = @Branch, Address1 = @Address1, Address2 = @Address2, City = @City, State = @State, ZipCode = @ZipCode, PhoneNumber = @PhoneNumber, EntryDate = @EntryDate, CustomerPrice = @CustomerPrice, ReturnedMdse = @ReturnedMdse, CustomerDisc = @CustomerDisc, Postage = @Postage, Tax = @Tax, TotalRefund = @TotalRefund, RefundType = @RefundType, Allergy = @Allergy, Notes = @Notes, EnteredBy = @EnteredBy WHERE VoucherNum = @VoucherNum;";
            int RowsUpdated;


            cmd = new SqlCommand(updatecmd, conn);
            cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            cmd.Parameters.AddWithValue("@Branch", dbBranch.SelectedValue);
            cmd.Parameters.AddWithValue("@Address1", txtAddress1.Text);
            cmd.Parameters.AddWithValue("@Address2", txtAddress2.Text);
            cmd.Parameters.AddWithValue("@City", txtCity.Text);
            cmd.Parameters.AddWithValue("@State", dbState.SelectedValue);
            cmd.Parameters.AddWithValue("@ZipCode", txtZip.Text);
            cmd.Parameters.AddWithValue("@PhoneNumber", txtPhoneNum.Text);
            cmd.Parameters.AddWithValue("@EntryDate", System.DateTime.Now);
            cmd.Parameters.AddWithValue("@CustomerPrice", txtRefund.Text);
            cmd.Parameters.AddWithValue("@ReturnedMdse", txtReturnedMdse.Text);
            cmd.Parameters.AddWithValue("@CustomerDisc", txtCustDiscount.Text);
            cmd.Parameters.AddWithValue("@Postage", txtPostage.Text);
            cmd.Parameters.AddWithValue("@Tax", txtTax.Text);
            cmd.Parameters.AddWithValue("@TotalRefund", txtTotal.Text);
            cmd.Parameters.AddWithValue("@VoucherNum", VoucherNum);
            cmd.Parameters.AddWithValue("@RefundType", dbRefType.SelectedValue);
            cmd.Parameters.AddWithValue("@Allergy", chkAllergy.Checked);
            cmd.Parameters.AddWithValue("@Notes", txtNotes.Text);
            cmd.Parameters.AddWithValue("@EnteredBy", User.Identity.Name);

            conn.Open();
            da.UpdateCommand = cmd;
            RowsUpdated = da.UpdateCommand.ExecuteNonQuery();

            cmd.Dispose();
            conn.Close();
            
            clearForm();
            Response.Write("Rows Updated: " + RowsUpdated.ToString());
            Response.Write("VoucherNum " + VoucherNum.ToString() + " has been modified.");
            Response.Redirect("~/PendingRefunds.aspx");

        }

        private void clearForm()
        {
            foreach (Control ctrl in form1.Controls)
            {
                if(ctrl is System.Web.UI.WebControls.TextBox)
                {
                    (ctrl as TextBox).Text = "";
                }

                if(ctrl is System.Web.UI.WebControls.DropDownList)
                {
                    (ctrl as DropDownList).SelectedIndex = 0;
                }

                if (ctrl is System.Web.UI.WebControls.CheckBox)
                {
                    (ctrl as CheckBox).Checked = false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PendingRefunds.aspx");
        }
    }
}