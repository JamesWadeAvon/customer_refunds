﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CustomerRefundsAsp
{
    public partial class PendingRefunds : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gridPending_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ModifyButton")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gridPending.Rows[index];
                Response.Redirect("~/ModifyPending.aspx?VoucherNum=" + row.Cells[1].Text);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}